# Suckless `st` Terminal Emulator

This is my patched version of the [Suckless `st`](https://st.suckless.org/) terminal emulator.

The patches applied are listed in the [patches](./patches) folder.
They were applied in the order of their numeric prefixes.

## >>**ALERT**<<

If I apply the [`xresources`](https://st.suckless.org/patches/xresources/) patch it breaks the program by not loading the requested font (either from the source code or the `.Xresources` file).

#### Autor

_Selan (<code.forager@gmail.com>), 05/Feb/2024 (&copy; 2024)._
